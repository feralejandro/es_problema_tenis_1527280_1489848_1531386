# -*- coding: utf-8 -*-
"""
Created on Thu May  7 20:47:37 2020

@author: Andrés Bitto (1527280), Alejandro Fernández (1489848), Meritxell Guzmán(1531386)
"""
class Player:
    def __init__(self, name: str):
        self.name = name
        self.num_points = 0
        
    def get_name(self) -> str:
        return self.name
    
    def get_points(self) -> int:
        return self.num_points
    
    def is_equal(self, player) -> bool:
        return self.name == player.name
    
    def add_point(self) -> None:
        self.num_points = self.num_points + 1
        
        