"""
Created on Thu May  7 20:47:37 2020

@author: Andrés Bitto (1527280), Alejandro Fernández (1489848), Meritxell Guzmán(1531386)
"""

import Player

class Game:

	def __init__(self, player1:str, player2: str):
		self.player1_name = player1
		self.player1_num_points = 0
		self.player2_name = player2
		self.player2_num_points = 0
		
	def get_score(self) -> str:
         if self.player1_num_points == self.player2_num_points and self.player1_num_points == 1:
             return "Fifteen-All"
         if self.player1_num_points == 1 and self.player2_num_points == 0:
             return "Fifteen-Love"
         if self.player1_num_points == self.player2_num_points and self.player1_num_points == 2:
             return "Thirty-All"
         if self.player1_num_points == self.player2_num_points and self.player1_num_points >= 3:
             return "Deuce"
         if self.player1_num_points == 0 and self.player2_num_points == 1:
             return "Love-Fifteen"
         if self.player1_num_points == 2 and self.player2_num_points == 0:
             return "Thirty-Love"
         if self.player1_num_points == 0 and self.player2_num_points == 3:
             return "Love-Forty"
         if self.player1_num_points > self.player2_num_points and self.player1_num_points - self.player2_num_points >= 2:
             return "Win for player1"
         if self.player1_num_points < self.player2_num_points and self.player2_num_points - self.player1_num_points >= 2:
             return "Win for player2"
         if self.player1_num_points == 2 and self.player2_num_points == 1:
             return "Thirty-Fifteen"
         if self.player1_num_points == 1 and self.player2_num_points == 2:
             return "Fifteen-Thirty"
         if  self.player1_num_points == 3 and self.player2_num_points == 1:
             return "Forty-Fifteen"
         if  self.player1_num_points == 1 and self.player2_num_points == 3:
             return "Fifteen-Forty"
         if  self.player1_num_points == 3 and self.player2_num_points == 2:
             return "Forty-Thirty"
         if  self.player1_num_points == 2 and self.player2_num_points == 3:
             return "Thirty-Forty"
         if self.player1_num_points > self.player2_num_points and self.player1_num_points - self.player2_num_points == 1:
             return "Advantage player1"
         if self.player1_num_points < self.player2_num_points and self.player2_num_points - self.player1_num_points == 1:
             return "Advantage player2"
         return "Love-All"
		
	
	def won_point(self, player: str) -> None:
		if player == "player1":
			self.player1_num_points = self.player1_num_points + 1
		else:
			self.player2_num_points = self.player2_num_points + 1